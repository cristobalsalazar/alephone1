import { Project } from "components/ProjectItem";
import { Social } from "components/SocialMediaLinks";
import { Event } from "components/Events";

export interface IndexProps {
  websiteTitle: string;
  name: string;
  about: {
    bio: string;
    email: string;
    profilePicture: string;
    slogan: string;
  };
  cover: {
    image: string;
    text: string;
    button: {
      text: string;
      link: string;
    };
  };
  projects: Project[];
  albums: Project[];
  events?: Event[];
  social: Social;
}

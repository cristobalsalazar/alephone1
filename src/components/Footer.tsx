import { Project } from "./ProjectItem";

type LinkData = {
  name: string;
  url: string;
};

type FooterSectionProps = {
  title: string;
  links: LinkData[];
};
const FooterSection: React.FC<FooterSectionProps> = (props) => (
  <div>
    <h2 className="mb-6 text-lg">{props.title}</h2>
    <ul>
      {props.links.map((link) => (
        <li key={link.name} className="mb-3 text-blue-400 hover:text-blue-500">
          <a href={link.url}>{link.name}</a>
        </li>
      ))}
    </ul>
  </div>
);

export type FooterProps = {
  albums: Project[];
  collaborations: Project[];
};

export const Footer: React.FC<FooterProps> = (props) => (
  <footer className="flex min-h-screen text-white bg-dark">
    <ul className="container flex m-auto justify-evenly">
      <li className="text-white">
        <FooterSection
          title="Albums"
          links={props.albums.map((album) => ({
            name: album.title,
            url: album.link,
          }))}
        />
      </li>
      <li className="text-white">
        <FooterSection
          title="Collaborations"
          links={props.collaborations.map((collab) => ({
            name: collab.title,
            url: collab.link,
          }))}
        />
      </li>
    </ul>
  </footer>
);

interface ScrollToLinkProps {
  title: string;
  target: string;
}
const ScrollToLink: React.FC<ScrollToLinkProps> = ({ target, title }) => {
  function handleClick() {
    const el = document.getElementById(target);
    window.scrollTo({
      left: 0,
      top: el.getBoundingClientRect().top + window.scrollY,
      behavior: "smooth",
    });
  }

  return (
    <div
      onClick={handleClick}
      className="py-2 text-lg transition-colors duration-300 cursor-pointer md:py-0 md:text-md hover:text-yellow-100"
    >
      {title}
    </div>
  );
};
export default ScrollToLink;

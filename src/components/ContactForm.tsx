import React, { FormHTMLAttributes, InputHTMLAttributes } from "react";
import CustomButton from "./CustomButton";
import CustomButtonInverse from "./CustomButtonInverse";

type InputProps = React.DetailedHTMLProps<
  InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

type FormProps = React.DetailedHTMLProps<
  FormHTMLAttributes<HTMLFormElement>,
  HTMLFormElement
>;

type FormLabelProps = {
  htmlFor: string;
};
const FormLabel: React.FC<FormLabelProps> = (props) => (
  <label htmlFor={props.htmlFor} className="mb-3 text-sm">
    {props.children}
  </label>
);

const SiteInput: React.FC<InputProps> = (props) => (
  <input className="w-full p-2 border focus:outline-none" {...props} />
);

const ContactForm: React.FC<FormProps> = () => (
  <form method="POST" name="contact" data-netlify="true">
    <p className="hidden">
      <label>
        Don’t fill this out if you're human: <input name="bot-field" />
      </label>
    </p>
    <div className="w-full mx-auto">
      <div className="flex flex-col mb-2">
        <FormLabel htmlFor="name">Name</FormLabel>
        <SiteInput name="name" required placeholder="John Doe" />
      </div>
      <input type="hidden" name="form-name" value="contact" />
      <div className="flex flex-col mb-2">
        <FormLabel htmlFor="email">Email</FormLabel>
        <SiteInput
          name="email"
          type="email"
          required
          placeholder="email@example.com"
        />
      </div>
      <div className="flex flex-col mb-2">
        <FormLabel htmlFor="message">Message</FormLabel>
        <textarea
          name="message"
          required
          placeholder="Enter your message here"
          className="w-full p-2 mb-6 border focus:outline-none"
          rows={4}
        />
      </div>
      <div className="flex">
        <div className="ml-auto">
          <CustomButtonInverse type="submit">Send</CustomButtonInverse>
        </div>
      </div>
    </div>
  </form>
);

export default ContactForm;

export interface Event {
  title: string;
  description?: string;
  link: string;
  linkName: string;
  location: string;
  date: string;
}

interface EventsProps {
  events: Event[];
}

const Events: React.FC<EventsProps> = ({ events }) => (
  <div>
    <h1 className="mb-6 text-xl">Upcoming Events</h1>
    <ul>
      {events.map((event) => (
        <li
          key={event.title}
          className="items-center justify-between w-full p-3 mb-3 text-black transition-colors duration-300 bg-white shadow md:rounded-lg md:flex hover:bg-gray-200"
        >
          <div className="flex flex-col">
            <p>{event.date}</p>
            <address>{event.location}</address>
          </div>
          <p>{event.description}</p>
          <a className="text-blue-500 underline" href={event.link}>
            {event.linkName}
          </a>
        </li>
      ))}
    </ul>
  </div>
);

export default Events;

import ContactForm from "./ContactForm";

interface AboutProps {
  slogan: string;
  bio: string;
  email: string;
  profilePicture: string;
}
const About: React.FC<AboutProps> = ({ slogan, bio, profilePicture }) => (
  <div className="items-center grid-cols-2 gap-8 pb-12 mx-auto lg:pb-0 lg:grid">
    <img
      className="object-contain w-full mb-6 lg:mb-0"
      src={profilePicture}
      alt="profile_picture"
    />
    <div className="max-w-xl px-2 mx-auto lg:px-0">
      <h2 className="mb-6 text-xl italic font-light text-center md:text-left">
        {slogan}
      </h2>
      <p className="w-full mb-6 prose-lg text-black text">{bio}</p>
    </div>
  </div>
);
export default About;

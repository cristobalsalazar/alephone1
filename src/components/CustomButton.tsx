const CustomButton: React.FC<any> = ({ children }) => (
  <button className="w-full p-2 text-lg text-center text-white transition-colors duration-500 border border-white cursor-pointer hover:bg-white hover:text-dark">
    {children}
  </button>
);
export default CustomButton;

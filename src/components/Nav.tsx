import { useEffect, useState } from "react";
import SocialMediaLinks, { Social } from "./SocialMediaLinks";
import MobileNav from "./MobileNav";
import ScrollToLink from "./ScrollToLink";

interface NavProps {
  name: string;
  social: Social;
}

const Nav: React.FC<NavProps> = (props) => {
  const [opacity, setOpacity] = useState(0);

  useEffect(() => {
    const scrollHandler = () => {
      setOpacity(Math.min(window.scrollY / window.innerHeight, 1) * 2);
    };
    window.addEventListener("scroll", scrollHandler);
    return () => window.removeEventListener("scroll", scrollHandler);
  }, []);

  return (
    <>
      <MobileNav name={props.name} social={props.social} />
      <div className="hidden lg:fixed lg:z-30 lg:items-center lg:w-full lg:flex">
        <div
          className="absolute z-10 w-full h-full shadow bg-dark"
          style={{ opacity }}
        ></div>

        <h1 className="relative z-20 py-3 ml-4 text-2xl text-white md:ml-0 md:px-8 md:flex-shrink-0">
          {props.name}
        </h1>

        <nav className="relative z-20 w-full">
          <ul className="flex flex-col px-3 text-gray-100 md:flex-row md:flex md:items-center md:justify-around md:w-full md:max-w-xs">
            <li>
              <ScrollToLink title="Projects" target="projects" />
            </li>
            <li>
              <ScrollToLink title="About" target="about" />
            </li>
            <li>
              <ScrollToLink title="Contact" target="contact" />
            </li>
            <li>
              <ScrollToLink title="Events" target="events" />
            </li>
          </ul>
        </nav>

        <SocialMediaLinks
          className="relative z-20 flex ml-auto mr-3 text-white justify-evenly min-w-4xl"
          social={props.social}
        />
      </div>
    </>
  );
};

export default Nav;

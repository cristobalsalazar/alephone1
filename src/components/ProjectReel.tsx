import { useEffect, useRef } from "react";
import ProjectItem, { Project } from "./ProjectItem";
import { useTrail, animated } from "react-spring";
import { useIntersection } from "react-use";
import clsx from "clsx";

const outSpring = {
  opacity: 0,
  transform: "translateY(30px)",
};

const inSpring = {
  opacity: 1,
  transform: "translateY(0px)",
};

export type ProjectReelProps = {
  projects: Project[];
  initColumns: number;
  sectionTitle: string;
};
const ProjectReel = (props: ProjectReelProps) => {
  const [trail, setTrail] = useTrail(props.projects.length, () => outSpring);
  const ref = useRef();
  const intersection = useIntersection(ref, {});

  useEffect(() => {
    if (intersection?.isIntersecting) {
      setTrail(inSpring);
    } else {
      setTrail(outSpring);
    }
  }, [intersection?.isIntersecting]);

  return (
    <div ref={ref} className="overflow-hidden">
      <h2 className="mb-6 text-xl text-center text-white lg:text-left">
        {props.sectionTitle}
      </h2>
      <div
        className={clsx(
          `lg:grid-cols-${props.initColumns}`,
          "md:gap-3 lg:grid-cols-2 md:grid-cols-2 md:grid md:m-0"
        )}
      >
        {trail.map((spring, i) => {
          const project = props.projects[i];
          return (
            <animated.div className="m-3 md:m-0" key={i} style={spring}>
              <ProjectItem project={project} />
            </animated.div>
          );
        })}
      </div>
    </div>
  );
};

export default ProjectReel;

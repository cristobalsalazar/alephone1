import CustomButton from "./CustomButton";
interface HeroProps {
  image: string;
  text: string;
  buttonText: string;
  buttonLink: string;
}

const Hero: React.FC<HeroProps> = ({ image, text, buttonText, buttonLink }) => (
  <>
    <div className="absolute z-0 w-full h-screen bg-dark"></div>
    {image && (
      <img
        className="absolute z-0 object-cover object-center w-full h-full opacity-50"
        src={image}
        alt="cover-image"
      />
    )}
    <div className="relative z-10 flex flex-col h-screen">
      <div className="m-auto">
        <h2 className="mb-3 text-4xl font-thin text-center text-white lg:text-6xl">
          {text}
        </h2>
      </div>
    </div>
  </>
);
export default Hero;

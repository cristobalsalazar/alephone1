export type Project = {
  title: string;
  link: string;
  img: string;
};

const ProjectItem = ({ project }: { project: Project }) =>
  project.img ? (
    <div className="relative w-full h-full">
      <a
        href={project.link}
        key={project.title}
        target="blank"
        className="flex flex-col items-center justify-center overflow-hidden bg-green-800"
      >
        <label className="absolute z-0 text-lg font-bold text-white">
          {project.title}
        </label>
        <img
          className="relative z-0 w-full transition-opacity duration-300 cursor-pointer hover:opacity-25"
          src={project.img || "/images/project-fallback.jpg"}
          alt={project.title}
        />
      </a>
    </div>
  ) : (
    <ProjectItemFallback project={project} />
  );
export default ProjectItem;

const ProjectItemFallback = ({ project }: { project: Project }) => {
  return (
    <div className="relative w-full h-full bg-green-800">
      <a href={project.link} key={project.title} target="blank">
        <div className="flex flex-col items-center justify-center">
          <label className="absolute z-10 text-lg font-bold text-center text-white pointer-events-none">
            {project.title}
          </label>
          <img
            className="relative z-0 w-full transition-opacity duration-300 cursor-pointer hover:opacity-25"
            src={"/images/project-fallback.jpg"}
            alt={project.title}
          />
        </div>
      </a>
    </div>
  );
};

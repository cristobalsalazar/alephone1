import React from "react";
import {
  AiOutlineTwitter,
  AiFillFacebook,
  AiFillInstagram,
  AiFillLinkedin,
} from "react-icons/ai";
import { FaBandcamp, FaSpotify } from "react-icons/fa";

export interface Social {
  spotify?: string;
  bandcamp?: string;
  twitter?: string;
  facebook?: string;
  instagram?: string;
  linkedin?: string;
}

type UlProps = React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLUListElement>,
  HTMLUListElement
>;

type SocialMediaIconProps = {
  href: string;
  icon: any;
};
const SocialMediaIcon: React.FC<SocialMediaIconProps> = ({
  href,
  icon: Icon,
}) => (
  <li className="mr-3">
    <a href={href} target="blank">
      <Icon size={32} />
    </a>
  </li>
);

export interface SocialMediaLinksProps extends UlProps {
  social: Social;
}

const SocialMediaLinks: React.FC<SocialMediaLinksProps> = ({
  social: { bandcamp, spotify, twitter, facebook, linkedin, instagram },
  ...rest
}) => (
  <ul {...rest}>
    {bandcamp && <SocialMediaIcon href={bandcamp} icon={FaBandcamp} />}
    {spotify && <SocialMediaIcon href={spotify} icon={FaSpotify} />}
    {twitter && <SocialMediaIcon href={twitter} icon={AiOutlineTwitter} />}
    {facebook && <SocialMediaIcon href={facebook} icon={AiFillFacebook} />}
    {instagram && <SocialMediaIcon href={instagram} icon={AiFillInstagram} />}
    {linkedin && <SocialMediaIcon href={linkedin} icon={AiFillLinkedin} />}
  </ul>
);
export default SocialMediaLinks;

import SocialMediaLinks from "./SocialMediaLinks";
import { useSpring, animated } from "react-spring";
import { useState } from "react";
import ScrollToLink from "./ScrollToLink";
import { Cross } from "hamburger-react";

const MobileNav: React.FC<any> = (props) => {
  const [isToggled, setToggle] = useState(false);
  const spring = useSpring({
    transform: isToggled ? "translateY(0)" : "translatey(-50vh)",
    opacity: isToggled ? 1 : 0,
  });

  const toggle = () => {
    setToggle(!isToggled);
  };

  return (
    <div className="fixed z-30 flex flex-col w-full rounded-b-lg shadow lg:hidden">
      <div className="relative z-20 flex items-center px-3 bg-dark">
        <Cross color="white" toggled={isToggled} toggle={setToggle} />
        <h1 className="py-3 ml-4 text-2xl text-white md:ml-0 md:px-8 md:flex-shrink-0">
          {props.name}
        </h1>
      </div>

      <animated.div style={spring} className="px-3 bg-black rounded-b-lg">
        <nav className="py-3">
          <ul className="flex flex-col text-white">
            <li onClick={toggle}>
              <ScrollToLink title="Projects" target="projects" />
            </li>
            <li onClick={toggle}>
              <ScrollToLink title="Events" target="events" />
            </li>
            <li onClick={toggle}>
              <ScrollToLink title="About" target="about" />
            </li>
          </ul>
        </nav>
        <SocialMediaLinks
          className="flex w-full py-5 text-white justify-evenly"
          social={props.social}
        />
      </animated.div>
    </div>
  );
};
export default MobileNav;

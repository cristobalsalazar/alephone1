const CustomButtonInverse: React.FC<any> = ({ children }) => (
  <button className="w-full p-2 text-lg text-center transition-colors duration-500 border cursor-pointer text-dark border-dark hover:bg-dark hover:text-white">
    {children}
  </button>
);
export default CustomButtonInverse;

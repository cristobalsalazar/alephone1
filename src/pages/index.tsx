import { IndexProps } from "types/index-props";
import Head from "next/head";
import props from "data.json";
import Events from "components/Events";
import About from "components/About";
import Nav from "components/Nav";
import Hero from "components/Hero";
import ContactForm from "components/ContactForm";
import ProjectReel from "components/ProjectReel";
import { Footer } from "components/Footer";

export const getStaticProps = () => ({ props });

export default function Home(props: IndexProps) {
  return (
    <div>
      <Head>
        <title>{props.websiteTitle}</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@100;300;400;600;700&display=swap"
          rel="stylesheet"
        />
      </Head>
      <Nav name={props.name} social={props.social} />

      <section id="cover">
        <Hero
          image={props.cover.image}
          text={props.cover.text}
          buttonLink={props.cover.button.link}
          buttonText={props.cover.button.text}
        />
      </section>

      <section id="projects" className="bg-dark">
        <div className="container py-12 mx-auto">
          <ProjectReel
            projects={props.albums}
            sectionTitle="Albums"
            initColumns={2}
          />
        </div>
      </section>

      <section id="collabs" className="bg-dark">
        <div className="container py-12 mx-auto">
          <ProjectReel
            projects={props.projects}
            sectionTitle="Collaborations"
            initColumns={4}
          />
        </div>
      </section>

      <section id="about" className="flex items-center my-12 lg:min-h-screen">
        <About
          bio={props.about.bio}
          email={props.about.email}
          profilePicture={props.about.profilePicture}
          slogan={props.about.slogan}
        />
      </section>

      <div className="items-center grid-cols-2 gap-8 bg-gray-200 lg:grid">
        <section id="contact" className="w-10/12 mx-auto">
          <h2 className="mb-6 text-4xl font-thin">Contact Me</h2>
          <ContactForm />
        </section>
        <img src="images/2018_04_12_Harold_Camacho-211.jpg"></img>
      </div>

      {props.events && props.events.length > 0 && (
        <section id="events" className="py-12">
          <div className="container mx-auto">
            <Events events={props.events} />
          </div>
        </section>
      )}
      <Footer albums={props.albums} collaborations={props.projects} />
    </div>
  );
}
